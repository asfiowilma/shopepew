from django.forms import ModelForm
from django import forms
from .models import PaymentDetails, Transaksi

class Form(ModelForm): 
    class Meta: 
        model = PaymentDetails
        fields = '__all__'
        exclude = ('date', 'transaksi', 'finalPrice')
        widgets = {
            'firstname': forms.TextInput(
				attrs={
					'class': 'form-control',
                    'placeholder': 'First Name'
					}
				),
            'lastname': forms.TextInput(
				attrs={
					'class': 'form-control',
                    'placeholder': 'Last Name'
					}
				),
            'address': forms.Textarea(
				attrs={
					'class': 'form-control',
                    'placeholder': 'Address'
					}
				),
            'postalcode': forms.TextInput(
				attrs={
					'class': 'form-control',
                    'placeholder': 'Postal Code'
					}
				),
            'phone': forms.TextInput(
				attrs={
					'class': 'form-control',
                    'placeholder': 'Phone'
					}
				),
            'promo': forms.TextInput(
				attrs={
					'class': 'form-control',
                    'label': 'Enter Promo Code'
					}
				),
			}