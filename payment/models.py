from django.db import models
from django.utils import timezone

# Create your models here.
class Transaksi(models.Model):
    name = models.CharField(max_length = 40)
    kodePromo = models.CharField(max_length=14, default="")

    finalPrice = models.IntegerField(default=0)
    date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        formatted_date = self.date.strftime("%d %b %Y at %H:%M:%S")
        return "Purchase by {} on {}".format(self.name, formatted_date) 

class PaymentDetails(models.Model): 
    date = models.DateTimeField(default=timezone.now)

    firstname = models.CharField(max_length=20,default="")
    lastname = models.CharField(max_length=20,default="")
    address = models.TextField(default="")
    postalcode = models.CharField(max_length=5, default="")
    phone = models.CharField(max_length=15, default="")

    promo = models.CharField(max_length=14, default="")
    finalPrice = models.IntegerField(default=0)

    transaksi = models.OneToOneField(Transaksi, on_delete=models.CASCADE)

    def __str__(self):
        return self.firstname + " " + self.lastname
