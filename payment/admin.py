from django.contrib import admin
from .models import PaymentDetails, Transaksi

# Register your models here.
admin.site.register(PaymentDetails)
admin.site.register(Transaksi)