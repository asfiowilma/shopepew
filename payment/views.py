from django.shortcuts import render, redirect
# import models
from django.contrib.auth.models import User
from products.models import Product, ShoppingCart
from .models import PaymentDetails, Transaksi
from discount.models import listDiscount
# import forms
from .forms import Form

from django.utils import timezone
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from django.contrib import messages

def validatePromo(kode, harga):
    try:
        validate = listDiscount.objects.get(kodePromo=kode)
    except:
        print("not found promo")
        return 1

    if validate.hargaMinimum > harga or validate.expiredDate < timezone.now():
        print("requirement not found")
        return 1

    return (100 - validate.persenDiskon)/100

@login_required
def checkout(request, id):
    user = User.objects.get(id = id)
    cart = ShoppingCart.objects.get(user = user) 
    products_in_cart = cart.as_dict()
    summary = [ product['subtotal'] for product in products_in_cart['products'] ]
    total = sum(summary)
    request.session['price'] = total
    request.session['discounted'] = total
    request.session['usedPromo'] = False

    if request.method == 'POST':
        form = Form(request.POST)
        print('berhasil ngepost', form.data, form.is_valid(), form.errors)   

        if form.is_valid():
            discount = request.POST['promo']
            harga = total

            discounted = total * validatePromo(discount, harga)
            request.session['discounted'] = discounted
            request.session['temp_data'] = form.cleaned_data
                     
            return redirect('payment:invoice', id = id)
    else:
        form = Form()

    context = {
        'form' : form, 
        'cart' : products_in_cart,
        'total' : total
        }

    return render(request, 'payment/checkout.html', context)

def invoice(request, id):
    user = User.objects.get(id = id)
    cart = ShoppingCart.objects.get(user = user) 
    products_in_cart = cart.as_dict()

    temp = request.session['temp_data']
    finalPrice = request.session['discounted']

    name = temp['firstname'] + ' ' + temp['lastname']
    kodePromo = temp['promo']
    
    # check stock 
    instock = True
    supplies = [ ( product['name'], product['qty']) for product in products_in_cart['products'] ]
    for i in range (len(supplies)): 
        supply = Product.objects.get( name= supplies[i][0] )
        if supply.stok > supplies[i][1]: 
            continue
        else: 
            instock = False
            messages.danger(request, f'{supplies[i][0]} is out of stock!')
            break
    
    if instock: 
        transaksiObj = Transaksi(name=name, kodePromo=kodePromo, finalPrice=finalPrice)
        transaksiObj.save()
        for product in cart.products.all():
            product.transaksi = transaksiObj
            product.save()
        cart.products.clear()  
        products_bought = transaksiObj.producttobuy_set.all()

        # create payment detail object
        PaymentDetails.objects.create(
            firstname=temp['firstname'], 
            lastname=temp['lastname'], 
            promo=temp['promo'], 
            phone=temp['phone'], 
            address=temp['address'], 
            postalcode=temp['postalcode'], 
            transaksi=transaksiObj
            )
    
    del request.session['temp_data']
    del request.session['discounted']
    
    return render(request, 'payment/checkout.html', {'transaksi': transaksiObj, 'bought': products_bought})

@csrf_exempt
def useCoupon(request):

    if not(request.session['usedPromo']):
        kode = request.POST['kode']
        finalPrice = request.session['price']
        print(kode, finalPrice)

        finalPrice *= validatePromo(kode, finalPrice)
        print(finalPrice)

        request.session['discounted'] = finalPrice

        if (validatePromo(kode, finalPrice) != 1):
            request.session['usedPromo'] = True
    else:
        finalPrice = request.session['discounted']

    return JsonResponse({'discountPrice' : finalPrice})

