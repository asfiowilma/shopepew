from django.urls import path
from .views import checkout, invoice, useCoupon

app_name = 'payment'

urlpatterns = [
    path('<int:id>', checkout, name="checkout"),
    path('invoice/<int:id>', invoice, name="invoice"),
    path('useCoupon', useCoupon, name="useCoupon"),
]
