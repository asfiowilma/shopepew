from django.test import TestCase, Client
from django.urls import reverse, resolve

from .models import PaymentDetails, Transaksi
from discount.models import listDiscount
from django.contrib.auth.models import User

from products.models import Product, ProductToBuy, ShoppingCart
from .views import validatePromo
from django.utils import timezone
from datetime import datetime, timedelta

# Create your tests here.
class PaymentTests(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = User.objects.create(username='valen', password ='tino_drakoz')
        self.user.save()
        self.checkouturl = reverse('payment:checkout', args=[0])
        self.createproduct = Product.objects.create(
            name = 'nendo',
            price = 100000,
            imglink = "https://tinyurl.com/w53cq97"         
        )
        self.producttobuy = ProductToBuy.objects.create(
            product = self.createproduct,
            qty = 2
        )
        self.createcart = ShoppingCart.objects.get(user = self.user)
        self.createcart.products.add(self.producttobuy)
        self.createtransaksi = Transaksi.objects.create(
            name = "a",
            # cart = self.createcart,
            kodePromo = "asd",
            finalPrice = 1
        )
        self.createtransaksi2 = Transaksi.objects.create(
            name = "a",
            # cart = self.createcart,
            kodePromo = "asd",
            finalPrice = 3
        )
        self.createpayment_jeremy = PaymentDetails.objects.create(
            # cart = self.createcart,
            firstname = 'jeremy',
            lastname = 'jeremy',
            address = 'indonesia',
            postalcode = '13230',
            phone = '08123123123',
            transaksi = self.createtransaksi
        )
        self.createpayment_valen = PaymentDetails.objects.create(
            # cart = self.createcart,
            firstname = 'valen',
            lastname = 'tino',
            address = 'bukan di indonesia',
            postalcode = '16084',
            phone = '0812456456',
            transaksi = self.createtransaksi2
        )        
        self.creatediscount = listDiscount.objects.create(
            expiredDate = timezone.now() + timedelta(hours=100),
            name = "shopepew",
            persenDiskon = 100,
            hargaMinimum = 100,
            kodePromo = "pew"
        )

    ## urls

    def test_checkout_GET(self):
        response = self.client.get(self.checkouturl)
        self.assertEquals(response.status_code, 302)

    ## models

    def test_nama_product(self):
        self.assertEquals(str(self.createproduct), "nendo")

    def test_checkout_valen(self):
        self.assertEquals(str(self.createpayment_valen), "valen tino")

    def test_checkout_jeremy(self):
        self.assertEquals(str(self.createpayment_jeremy.firstname), "jeremy")
        self.assertEquals(str(self.createpayment_jeremy), "jeremy jeremy")

    def test_transaksi(self):
        formatted_date = self.createtransaksi.date.strftime("%d %b %Y at %H:%M:%S")
        self.assertEqual(str(self.createtransaksi), f"Purchase by a on {formatted_date}")

    ## views

    def test_validate_promo(self):
        self.assertEqual(validatePromo(1,1), 1)
        self.assertEqual(validatePromo("pew",0.5),1)
        self.assertEqual(validatePromo("pew",10000),0.0)

    def test_checkout_POST(self):
        response = self.client.post(self.checkouturl, {
        'firstname' : "jere",
        'lastname' : "jereh",
        'address' : "depok",
        'postalcode' : "17420",
        'phone' : "0812",
        'promo' : "pew"
        })

