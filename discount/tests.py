from django.test import TestCase, Client
from django.urls import reverse
from .models import listDiscount
from .views import discount
from datetime import datetime

# Create your tests here.
class DiscountTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.discount_url = reverse('discount:discount')
        self.creatediscount = listDiscount.objects.create(
            expiredDate = datetime.now(),
            name = "shopepew",
            persenDiskon = 100,
            hargaMinimum = 100,
            kodePromo = "pew"
        )
    
    ## Models
    def test_listDiscount(self):
        self.assertEquals(str(self.creatediscount),"shopepew")
        
    ## Views
    def test_discount_GET(self):
        response = self.client.get(self.discount_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "promo.html")

    def test_discount_POST(self):
        response = self.client.post(self.discount_url, {
            'category' : 'persenDiskon',
            'text' : 10
        })
        response2 = self.client.post(self.discount_url, {
            'category' : 'kodePromo',
            'text' : 'pew'
        })
        self.assertContains(response, 10)
        self.assertContains(response2, 'pew')