from django.urls import path
from .views import discount, listJSON

app_name = "discount"

urlpatterns = [
    path('', discount, name='discount'),
    path('listJSON/<int:persen>', listJSON, name='listJSON'),
    path('listJSON/', listJSON, name='listJSON'),
]