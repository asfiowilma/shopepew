from django.shortcuts import render, redirect
from .models import listDiscount, promoSearch
from django.http import JsonResponse

# Create your views here.
def discount(request):
    promos = listDiscount.objects.all()

    if request.method == 'POST':
        searchForm = promoSearch(request.POST)

        if searchForm.is_valid():
            category = request.POST.get('category')
            text = request.POST.get('text')
            
            if category == "kodePromo":
                promos = listDiscount.objects.filter(kodePromo = text)
            else:
                promos = listDiscount.objects.filter(persenDiskon__gte = text)

    searchForm = promoSearch()

    return render(request, 'promo.html', {'promos' : promos, 'form' : searchForm})

def listJSON(request, persen = 100):

    promos = listDiscount.objects.filter(persenDiskon__gte = persen).values()

    return JsonResponse({'promosDict' : list(promos)})