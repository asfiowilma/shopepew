from django.test import TestCase, Client, LiveServerTestCase
from django.urls import reverse

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait

from discount.models import listDiscount
from products.models import Product

from datetime import datetime
import time


# Create your tests here.
class TestHome(TestCase):
    def setUp(self):
        self.client = Client()
        self.discount_url = reverse('homepage:index')
        self.test_product = Product.objects.create(
            name = "baju",
            price = 100,
            desc = "baju mahal",
            stok = 1,
            imglink = "www.fb.com"
        )
        self.creatediscount = listDiscount.objects.create(
            expiredDate = datetime.now(),
            name = "shopepew",
            persenDiskon = 100,
            hargaMinimum = 100,
            kodePromo = "pew"
        )
    
    def test_homepage(self):
        response = self.client.get(self.discount_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "homepage/index.html")

# functional test for the whole website
""" class ShopepewTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser =  webdriver.Chrome('./chromedriver',chrome_options = chrome_options)
    
    def tearDown(self): 
        self.browser.quit()
    
    def test_shopepew_functional_test(self): 
        # Leo opens the homepage 
        self.browser.get(self.live_server_url)
        self.browser.implicitly_wait(10)
        
        # and sees a signup button 

        # Leo fills in his desired username, and entered the password twice 
        
        # He clicks register

        # He now wants to log in
        # Leo reenters the username he registered earlier, including the password 

        # Leo clicks login 

        # Leo browses the website for anything he liked 
        # He noticed something that he knows he needed
        # So Leo immediately clicked add to cart on that product

        # Satisfied, he clicked one of the other products 

        # Leo gets directed to the other product's info page 
        # where he sees the product's name and price

        # Leo decides to buy the product, he clicks buy 

        # He gets redirected to the checkout page
        # Leo sees the first product he added by clicking add to cart 
        # and the other product he chose by clicking buy now on the product's page

        # Leo fills in all the informations that was asked by the page

        # He entered a coupon code that was suggested by the website

        # And last, he clicked checkout 

        # Leo sees a modal pops up showing him the summary of the transaction 

        # After making sure everything is okay, Leo clicked confirm

        # The page notifies him that the transaction went well 
        # Satisfied, Leo logged out of his account  """