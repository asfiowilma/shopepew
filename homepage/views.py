from django.shortcuts import render
from discount.models import listDiscount
from products.models import Product

def index(request):
    promos = listDiscount.objects.all()[:3]
    products = Product.objects.all()[:6]
    context = {
        'home':'yes',
        'promos': promos,
        'items': products
    }
    return render(request, 'homepage/index.html', context)