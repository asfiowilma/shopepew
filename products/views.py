from django.shortcuts import render
from django.http import JsonResponse

from django.views.decorators.csrf import csrf_exempt

from .models import Product, ProductToBuy, ShoppingCart
from review.models import Review
from django.contrib.auth.models import User


def products(request):
    items = Product.objects.all()
    context = {'items':items}

    if request.method == 'POST':
        query = request.POST['product_name']
        allitems = items.filter(name__icontains=query)
        context['query'] = query
        context['allitems'] = allitems

        return render(request, 'searchresult.html', context)

    return render(request, 'product.html', context)

def product_byid(request, num):
    items = Product.objects.get(id=num)
    review = Review.objects.order_by('date').first()

    context = {'items' : items, 'review' : review}
    return render(request, 'productbyid.html', context)

@csrf_exempt
def addToCart(request):
    if request.method == 'POST':
        user = request.POST['user']
        product = request.POST['product']       

        shopper = User.objects.get(username=user)
        cart = ShoppingCart.objects.get(user=shopper)

        new_product = Product.objects.get(id=product)        
        to_buy, new = cart.products.get_or_create(product=new_product)
        # adds new product to existing shopping cart
        if new: 
            cart.products.add(to_buy)

        # adds qty of a product already in shopping cart 
        else: 
            to_buy.qty += 1
            to_buy.save()
            
        return JsonResponse(to_buy.as_dict())

@csrf_exempt
def tambahKurangCart(request):
    if request.method == 'POST':
        user = request.POST['user']  
        id = request.POST['id']
        add = bool(request.POST['add'])

        shopper = User.objects.get(username=user)
        cart = ShoppingCart.objects.get(user=shopper)

        new_product = Product.objects.get(id=id)        
        to_buy, new = ProductToBuy.objects.get_or_create(product=new_product)

        # increase qty
        if add: 
            to_buy.qty += 1
            to_buy.save()

        # decrease qty
        else: 
            to_buy.qty -= 1
            to_buy.save()
            
            if to_buy.qty == 0: 
                cart.products.remove(to_buy)
        
    return JsonResponse(to_buy.as_dict())

@csrf_exempt
def grabTotal(request): 
    if request.method == 'POST':
        user = request.POST['user']  
        shopper = User.objects.get(username=user)
        cart = ShoppingCart.objects.get(user=shopper)

        products_in_cart = cart.as_dict()
        summary = [ product['subtotal'] for product in products_in_cart['products'] ]
        total = sum(summary)

    return JsonResponse({'total': total})