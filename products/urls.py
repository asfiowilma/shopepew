from django.urls import path
from .views import products, product_byid, addToCart, tambahKurangCart, grabTotal
from django.conf.urls import url

app_name = 'product'

urlpatterns = [
    path('', products, name='index'),
    path('<int:num>', product_byid, name='id'),
    path('add/', addToCart, name='add'),
    path('tbhkrg/', tambahKurangCart, name='tbhkrg'),
    path('grabtotal/', grabTotal, name='grabtotal')
]