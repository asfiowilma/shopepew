from django.db import models
from django.forms import ModelForm
# import models
from django.contrib.auth.models import User
from payment.models import Transaksi

from django.db.models.signals import post_save
from django.dispatch import receiver

class Product(models.Model):
    name = models.CharField(max_length=50)
    price = models.IntegerField()
    desc = models.CharField(max_length=400, default="")
    stok = models.IntegerField(default=10)
    imglink = models.CharField(max_length=200, default="https://shopepew.herokuapp.com/static/homepage/logo%20white%201.png")

    def __str__(self):
        return self.name

class ProductToBuy(models.Model):
    product = models.ForeignKey(Product, blank=True, on_delete=models.CASCADE)
    qty = models.IntegerField(default=1)
    transaksi = models.ForeignKey(Transaksi, null=True, on_delete=models.CASCADE)

    def as_dict(self):
        return {
            'id' : self.product.id,
            'name': self.product.name,
            'price': self.product.price,
            'qty': self.qty,
            'subtotal': self.product.price * self.qty,
            'imglink': self.product.imglink
        }
    
    def __str__(self): 
        try:
            username = self.shoppingcart_set.get().user.username
        except:
            try: 
                username = self.transaksi.name
            except:
                username = "Someone"
        return self.product.name + " for " + username

class ShoppingCart(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True)
    products = models.ManyToManyField(ProductToBuy, blank=True)

    def as_dict(self): 
        return { 'products': [product.as_dict() for product in self.products.all()] }
    
    def __str__(self):
        return f"{self.user.username}'s Shopping Cart"

@receiver(post_save, sender=User)
def create_shopping_cart(sender, instance, created, **kwargs): 
    if created:
        ShoppingCart.objects.create(user=instance)

class Product_Form(ModelForm):
    class Meta:
        model = Product
        fields = '__all__'

