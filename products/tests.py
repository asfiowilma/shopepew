from django.test import TestCase, Client
from django.urls import reverse, resolve

from .views import addToCart
from .models import Product, ProductToBuy, ShoppingCart
from review.models import Review
from django.contrib.auth.models import User

from django.utils import timezone

class TestViews(TestCase):
    def setUp(self):
        self.client = Client()
        self.product_url = reverse("product:index")
        self.test_product = Product.objects.create(
            name = "baju",
            price = 100,
            desc = "baju mahal",
            stok = 1,
            imglink = "www.fb.com"
        )
        self.test_review = Review.objects.create(
            reviewer = 'jerri',
            bintang = 3,
            product = self.test_product,
            msg = "barang terkutuk",
            date = timezone.now(),
        )
        self.productID_url = reverse("product:id", args=[self.test_product.id])

    def test_product_GET(self):
        response = self.client.get(self.product_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "product.html")

    def test_productID_GET(self):
        response = self.client.get(self.productID_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "productbyid.html")

    def test_product_POST(self):
        response = self.client.post(self.product_url, {
            "product_name" : "kaos"
        })
        self.assertContains(response, "kaos")
        self.assertTemplateUsed(response, "searchresult.html")

    def test_product_MODEL(self):
        self.assertEqual(str(Product.objects.first()), "baju")

class ShoppingCartTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.product_url = reverse("product:index")
        self.user = User.objects.create(username='valen', password ='tino_drakoz')

        self.test_product1 = Product.objects.create(
            name = "Dalgona Latte",
            price = 100,
            desc = "Frothy kopi kekinian",
            stok = 1,
            imglink = "www.dalgonalatte.com"
        )        
        self.test_product2 = Product.objects.create(
            name = "Brown Sugar Boba",
            price = 100,
            desc = "BSB is so yesterday",
            stok = 1,
            imglink = "www.brownsugarboba.com"
        )
        self.test_product3 = Product.objects.create(
            name = "Kopi Kenangan",
            price = 100,
            desc = "Kopi mahal",
            stok = 1,
            imglink = "www.kenanganterindah.com"
        )
        self.test_product1.save()
        self.test_product2.save()
        self.test_product3.save()        
    
    def test_shopping_cart_model(self):
        cart, status = ShoppingCart.objects.get_or_create(user=self.user)
        p1 = ProductToBuy.objects.create(product=self.test_product1, qty=1)
        p2 = ProductToBuy.objects.create(product=self.test_product2, qty=1)
        p3 = ProductToBuy.objects.create(product=self.test_product3, qty=1)
        cart.products.add(p1, p2, p3)
        
        cart_objects = cart.products.all()

        dalgona = cart_objects.get(product=self.test_product1)
        boba = cart_objects.get(product=self.test_product2)
        kenangan = cart_objects.get(product=self.test_product3)

        self.assertEquals(dalgona.product.name, "Dalgona Latte")
        self.assertEquals(boba.product.name, "Brown Sugar Boba")
        self.assertEquals(kenangan.product.name, "Kopi Kenangan")

        card_dict = { 'products' : 
            [
                {'id': 1,
                'name': "Dalgona Latte",
                'price': 100,
                'qty': 1,
                'subtotal': 100,
                'imglink': "www.dalgonalatte.com"},

                {'id': 2,
                'name': "Brown Sugar Boba",
                'price': 100,
                'qty': 1,
                'subtotal': 100,
                'imglink': "www.brownsugarboba.com"},

                {'id': 3,
                'name':"Kopi Kenangan",
                'price': 100,
                'qty': 1,
                'subtotal': 100,
                'imglink': "www.kenanganterindah.com"}
            ]
        }

        self.assertEquals(cart.as_dict(), card_dict)
    
    def test_add_to_cart_view(self):
        found = resolve(reverse('product:add'))
        self.assertEqual(found.func, addToCart)
    
    def test_add_to_cart_post(self):
        # make sure the user is logged in
        self.client.post(reverse('login'), data={
            'username': 'valen',
            'password': 'tino_drakoz',
        })
        # post to add to cart view
        self.client.post(reverse('product:add'), {
            "user" : "valen",
            "product" : 1,
        })
        self.client.post(reverse('product:add'), {
            "user" : "valen",
            "product" : 3,
        })
        cart = ShoppingCart.objects.get(user=self.user)
        products = cart.products.all()

        self.assertEquals(cart.user.username, "valen")
        self.assertEquals(str(cart), "valen's Shopping Cart")
        self.assertEquals(products[0].product, self.test_product1)
        self.assertEquals(products[1].product, self.test_product3)




