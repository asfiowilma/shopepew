from django.contrib import admin
from .models import Product, ProductToBuy, ShoppingCart

admin.site.register(Product)
admin.site.register(ProductToBuy)
admin.site.register(ShoppingCart)