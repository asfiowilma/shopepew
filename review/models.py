from django.db import models
from products.models import Product
from django.utils import timezone
from django.forms import ModelForm
from django.core.validators import MaxValueValidator, MinValueValidator

# Create your models here.
class Review(models.Model):

    reviewer = models.CharField(max_length=300,default="")
    bintang = models.IntegerField(default = 0, validators=[MaxValueValidator(5), MinValueValidator(1)])
    product = models.ForeignKey(Product, on_delete=models.CASCADE, default = 1)
    msg = models.TextField(default = "")
    date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.reviewer

class ReviewForm(ModelForm): 
    class Meta: 
        model = Review
        fields = '__all__'
        exclude = ('date', )