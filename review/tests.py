from django.test import TestCase, Client
from django.urls import reverse
from django.utils import timezone
import datetime
from .models import Review
from products.models import Product

# Create your tests here.
class ReviewTest(TestCase):
    # def test_if_review_bar_exists(self):
    #     response = Client().get('/')
    #     content = response.content.decode('utf8')
    #     self.assertIn("What they said.",content)

    # def test_review_model(self):
    #     Review.objects.create(
    #         msg = "Recommended!",
    #         reviewer = "wasi",
    #     )
    #     self.assertEqual(Review.objects.all().count(), 1)

    # def test_review_string_representation(self):
    #     review = Review(
    #         msg = "Recommended!",
    #         reviewer = "wasi",
    #     )

    #     self.assertEqual(str(review),review.review_msg)

    # def test_review(self):
    #     response = Client().post('/review',data={'msg':'bagus banget ihhhh!!!!!'}, content_type="application/json")
    #     #self.assertEqual(Review.objects.all().count(),1)
    #     self.assertTemplateUsed('review.html')
    #     self.assertEqual(response.status_code, 301)

    def setUp(self):
        self.client = Client()
        self.product_url = reverse("review:review")
        self.test_product = Product.objects.create(
            name = "baju",
            price = 100,
            desc = "baju mahal",
            stok = 1,
            imglink = "www.fb.com"
        )
        self.test_review = Review.objects.create(
            reviewer = 'ganigans',
            bintang = 4,
            product = self.test_product,
            msg = "saya cukup puas",
            date = timezone.now()
        )

    ## Model
    def test_review_model(self):
        self.assertEqual(str(self.test_review), 'ganigans')

    # ## Views
    # def test_review(self):
    #     response = self.client.post(self.test_product,{
    #         'reviewer' : 'ganiguns',
    #         'bintang' : 3,
    #         'product' : self.test_product,
    #         'msg' : "saya kurang puas",
    #         'date' : timezone.now()
    #     })
    #     self.assertContains(response,"keren")