from django.urls import path
from .views import reviews

app_name = 'review'

urlpatterns = [
    path('', reviews, name="review"),
]
