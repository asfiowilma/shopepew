from django.shortcuts import render, redirect
from .models import Review, ReviewForm

# Create your views here.
def reviews(request):
    reviews = Review.objects.all()

    if request.method == 'POST':
        form = ReviewForm(request.POST)

        if form.is_valid():
            form.save()

            return redirect('review:reviews')
    else:
        form = ReviewForm()

    return render(request, 'reviews/review.html', {'reviews' : reviews, 'form' : form})